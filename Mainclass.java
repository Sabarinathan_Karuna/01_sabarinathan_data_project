import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.*; 

class TeamWinner{
    int year;
    String teamName;
    public TeamWinner(int year, String teamName){
        this.year = year;
        this.teamName = teamName;
    }
}

class Mainclass{
    public static void function_3(){
        System.out.println("For the year 2016 get the extra runs conceded per team.");
        String csvFile = "/Users/shabrikaruna/Desktop/01_sabarinathan_data_project/ipl/deliveries.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";
        HashMap<String,Integer> map3 = new HashMap<>();
        try {

            br = new BufferedReader(new FileReader(csvFile));
            br.readLine();
            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] lines = line.split(cvsSplitBy);
                if(Integer.parseInt(lines[0]) >= 577){
                    if(map3.containsKey(lines[3])){
                        map3.put( lines[3] , map3.get(lines[3])+Integer.parseInt(lines[16]) );
                    }else{
                        map3.put( lines[3] , Integer.parseInt(lines[16]));
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        // for (Map.Entry<String,Integer> entry : map3.entrySet()) {
        //     System.out.println(entry.getKey() + " : "+ entry.getValue());
        // }

        map3.forEach((k, v) -> System.out.println(k +" , "+ v));

        


    }
	public static void main(String[] args) {

        String csvFile = "/Users/shabrikaruna/Desktop/01_sabarinathan_data_project/ipl/matches.csv";
        String csvFile_Deliveries = "/Users/shabrikaruna/Desktop/01_sabarinathan_data_project/ipl/deliveries.csv";
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";
        ArrayList<String> yearCount = new ArrayList<String>();
        HashMap<Integer, Integer> hashYear = new HashMap<Integer,Integer>();
        ArrayList<TeamWinner> teams = new ArrayList<TeamWinner>();
        ArrayList<Integer> idOf2017 = new ArrayList<Integer>();
		try {
            br = new BufferedReader(new FileReader(csvFile));
            br.readLine();
			while ((line = br.readLine()) != null) {
				String[] year = line.split(cvsSplitBy);
                // System.out.println("Matches [id= " + year[1] + " , season=" + year[1] + "]");
                yearCount.add(year[1]);
                int yearInt = Integer.parseInt(year[1]);
                teams.add(new TeamWinner(yearInt, year[10]));

                if(year[1].equals("2017")){
                    idOf2017.add(Integer.parseInt(year[0]));
                }
            }
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
        }

        TreeSet<String> unique = new TreeSet<String>(yearCount);

        for(String s : unique){
            int count = Collections.frequency(yearCount, s);
            hashYear.put( Integer.parseInt(s) , count);
    }
        System.out.println("1st Problem");
        System.out.println("-------------------------------------------------------------");
        System.out.println("NO OF MATCHES PLAYED IN EACH YEAR ");
        System.out.println("-------------------------------------------------------------");
        for (Map.Entry<Integer,Integer> entry : hashYear.entrySet()) {
            System.out.println("No of Matches in " + entry.getKey() + " is : "+ entry.getValue());
        }
        System.out.println();


        //2nd Problem


        String[] iplTeams  = new String[]{"Chennai Super Kings","Deccan Chargers","Delhi Daredevils","Gujarat Lions","Kings XI Punjab",
        "Kochi Tuskers Kerala","Kolkata Knight Riders","Mumbai Indians","Pune Warriors","Rajasthan Royals","Rising Pune Supergiant",
        "Royal Challengers Bangalore","Sunrisers Hyderabad"};
            
        Object obj= Collections.min(yearCount);      
        String minString = (String)obj;
        int minInt = Integer.parseInt(minString);
        System.out.println("2nd Problem");
        System.out.println("-------------------------------------------------------------");
        int count = 0;
        for(int k=0;k<unique.size();k++){
            int[] wins = new int[iplTeams.length]; 
            for(int i=0;i<teams.size();i++){
                TeamWinner t = (TeamWinner)teams.get(i);
                if(t.year == minInt){
                    if(t.teamName.equals(iplTeams[0])){
                        wins[0]+=1;
                    }else if(t.teamName.equals(iplTeams[1])){
                        wins[1]+=1;
                    }else if(t.teamName.equals(iplTeams[2])){
                        wins[2]+=1;
                    }else if(t.teamName.equals(iplTeams[3])){
                        wins[3]+=1;
                    }else if(t.teamName.equals(iplTeams[4])){
                        wins[4]+=1;
                    }else if(t.teamName.equals(iplTeams[5])){
                        wins[5]+=1;
                    }else if(t.teamName.equals(iplTeams[6])){
                        wins[6]+=1;
                    }else if(t.teamName.equals(iplTeams[7])){
                        wins[7]+=1;
                    }else if(t.teamName.equals(iplTeams[8])){
                        wins[8]+=1;
                    }else if(t.teamName.equals(iplTeams[9])){
                        wins[9]+=1;
                    }else if(t.teamName.equals(iplTeams[10])){
                        wins[10]+=1;
                    }else if(t.teamName.equals(iplTeams[11])){
                        wins[11]+=1;
                    }else if(t.teamName.equals(iplTeams[12])){
                        wins[12]+=1;
                    }
                }  
            }
            System.out.println("YEAR : "+minInt);
            System.out.println("-------------------------------------------------------------");
            for(int j=0;j<wins.length;j++){
                System.out.println(+minInt+" Teams : "+iplTeams[j] +" won : "+wins[j]);
            }
            System.out.println("-------------------------------------------------------------");
            minInt++;
        }

        // obj = Collections.min(idOf2017);      
        // int min1 = (int)obj;
        // System.out.println("MIN : "+min1);

        // obj = Collections.max(idOf2017);      
        // int max1 = (int)obj;
        // System.out.println("MAX: "+max1);
        System.out.println("3rd Problem");
        function_3();
        



    }
}